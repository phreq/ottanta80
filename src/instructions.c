#include "cpu.h"

// RLC
void rlc(registers *reg) {
    reg->flags.c = reg->a >> 7;					// carry = MSB
    reg->a = (reg->a << 1) | reg->flags.c;		// accumulator = accumulator shifted left by 1 with d0 moved to d7
}

// RAL
void ral(registers *reg) {
	uint8_t d7 = reg->flags.c;					// d7 = carry bit
	reg->flags.c = reg->a >> 7;					// carry = MSB
	reg->a = (reg->a << 1) | d7;				// accumulator = accumulator shifted left by 1 with carry bit shifted to d0
}

// RRC
void rrc(registers *reg) {
	reg->flags.c = reg->a & 1;					// carry = LSB
	reg->a = (reg->a >> 1) | (reg->a << 7);		// accumulator = accumulator shifted right by 1 with d0 moved to d7 
}

// RAR
void rar(registers *reg) {
	uint8_t d0 = reg->flags.c;					// d0 = carry bit
	reg->flags.c = reg->a & 1;					// carry = LSB
	reg->a = (reg->a >> 1) | (d0 << 7);			// accumulator = accumulator shifted right by 1 with carry bit shifted to d7
}
