#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "cpu.h"

int main(int argc, char *argv[]) {
	registers r;
	registers *reg = &r;
	uint16_t offset;
	FILE *fp = fopen(argv[1], "rb");
	int counter;

	mem = calloc(MEM_SIZE, sizeof(uint8_t));
	offset = 0x100;
	reg->pc = offset; 

	reg->a = 0;
	reg->b = 0;
	reg->c = 0;
	reg->d = 0;
	reg->e = 0;
	reg->h = 0;
	reg->l = 0;

	if (load_into_mem(mem, fp, offset) != 0) { // load file fp into mem at offset
		return 1;
	}

	counter = get_cycles(mem, fp, offset); 
	printf("cycles before execution: %d\n", counter);

	for (;;) {
		reg->ir = mem[reg->pc];

		switch (reg->ir) {
			// MISC/CONTROL INSTRUCTIONS
			case 0x00: break; // NOP
			case 0x76: break; // implement HLT when interrupts are implemented
			case 0xd3: break; // implement OUT when I/O ports are implemented
			case 0xdb: break; // implement IN when I/O ports are implemented 
			case 0xf3: break; // implement DI when interrupts are implemented
			case 0xfb: break; // implement EI when interrupts are implemented

			// 8-BIT LOAD/STORE/MOVE INSTRUCTIONS
			case 0x02: set_reg_pair(reg, BC, reg->a); break;							// STAX B
			case 0x12: set_reg_pair(reg, DE, reg->a); break;							// STAX D
			case 0x32: write_word(mem, read_word(mem, reg->pc+1), reg->a); break;		// STA a16
			case 0x06: reg->b = mem[reg->pc+1]; break;									// MVI B, d8
			case 0x16: reg->d = mem[reg->pc+1]; break;									// MVI D, d8
			case 0x26: reg->h = mem[reg->pc+1]; break;									// MVI H, d8
			case 0x36: set_reg_pair(reg, HL, mem[reg->pc+1]); break;					// MVI M, d8
			case 0x0e: reg->c = mem[reg->pc+1]; break;									// MVI C, d8
			case 0x1e: reg->e = mem[reg->pc+1]; break;									// MVI E, d8
			case 0x2e: reg->l = mem[reg->pc+1]; break;									// MVI L, d8
			case 0x3e: reg->a = mem[reg->pc+1]; break;									// MVI A, d8
			case 0x0a: reg->a = CONCAT(reg->b, reg->c); break;							// LDAX B
			case 0x1a: reg->d = CONCAT(reg->d, reg->e); break;							// LDAX D
			case 0x3a: reg->a = read_byte(mem, read_word(mem, reg->pc+1)); break;		// LDA a16
			case 0x40: reg->b = reg->b; break; 											// MOV B, B
			case 0x41: reg->b = reg->c; break; 											// MOV B, C
			case 0x42: reg->b = reg->d; break; 											// MOV B, D
			case 0x43: reg->b = reg->e; break; 											// MOV B, E
			case 0x44: reg->b = reg->h; break; 											// MOV B, H
			case 0x45: reg->b = reg->l; break;											// MOV B, L
			case 0x46: reg->b = CONCAT(reg->h, reg->l); break;							// MOV B, M
			case 0x47: reg->b = reg->a; break;											// MOV B, A
			case 0x48: reg->c = reg->b; break;											// MOV C, B
			case 0x49: reg->c = reg->c; break;											// MOV C, C
			case 0x4a: reg->c = reg->d; break;											// MOV C, D
			case 0x4b: reg->c = reg->e; break;											// MOV C, E
			case 0x4c: reg->c = reg->h; break;											// MOV C, H
			case 0x4d: reg->c = reg->l; break;											// MOV C, L
			case 0x4e: reg->c = CONCAT(reg->h, reg->l); break;							// MOV C, M
			case 0x4f: reg->c = reg->a; break;											// MOV C, A
			case 0x50: reg->d = reg->b; break;											// MOV D, B
			case 0x51: reg->d = reg->c; break;											// MOV D, C
			case 0x52: reg->d = reg->d; break;											// MOV D, D
			case 0x53: reg->d = reg->e; break;											// MOV D, E
			case 0x54: reg->d = reg->h; break;											// MOV D, H
			case 0x55: reg->d = reg->l; break;											// MOV D, L
			case 0x56: reg->d = CONCAT(reg->h, reg->l); break;							// MOV D, M
			case 0x57: reg->d = reg->a; break;											// MOV D, A
			case 0x58: reg->e = reg->b; break;											// MOV E, B
			case 0x59: reg->e = reg->c; break;											// MOV E, C
			case 0x5a: reg->e = reg->d; break;											// MOV E, D
			case 0x5b: reg->e = reg->e; break;											// MOV E, E
			case 0x5c: reg->e = reg->h; break;											// MOV E, H
			case 0x5d: reg->e = reg->l; break;											// MOV E, L
			case 0x5e: reg->e = CONCAT(reg->h, reg->l); break;							// MOV E, M
			case 0x5f: reg->e = reg->a; break;											// MOV E, A
			case 0x60: reg->h = reg->b; break;											// MOV H, B
			case 0x61: reg->h = reg->c; break;											// MOV H, C
			case 0x62: reg->h = reg->d; break;											// MOV H, D
			case 0x63: reg->h = reg->e; break;											// MOV H, E
			case 0x64: reg->h = reg->h; break;											// MOV H, H
			case 0x65: reg->h = reg->l; break;											// MOV H, L
			case 0x66: reg->h = CONCAT(reg->h, reg->l); break;							// MOV H, M
			case 0x67: reg->h = reg->a; break;											// MOV H, A
			case 0x68: reg->l = reg->b; break;											// MOV L, B
			case 0x69: reg->l = reg->c; break;											// MOV L, C
			case 0x6a: reg->l = reg->d; break;											// MOV L, D
			case 0x6b: reg->l = reg->e; break;											// MOV L, E
			case 0x6c: reg->l = reg->h; break;											// MOV L, H
			case 0x6d: reg->l = reg->l; break;											// MOV L, L
			case 0x6e: reg->l = CONCAT(reg->h, reg->l); break;							// MOV L, M
			case 0x6f: reg->l = reg->a; break;											// MOV L, A
			case 0x70: set_reg_pair(reg, HL, reg->b); break;							// MOV M, B
			case 0x71: set_reg_pair(reg, HL, reg->c); break;							// MOV M, C
			case 0x72: set_reg_pair(reg, HL, reg->d); break;                            // MOV M, D
			case 0x73: set_reg_pair(reg, HL, reg->e); break;                            // MOV M, E
			case 0x74: set_reg_pair(reg, HL, reg->h); break;                            // MOV M, H
			case 0x75: set_reg_pair(reg, HL, reg->l); break;                            // MOV M, L
			case 0x77: set_reg_pair(reg, HL, reg->a); break;                            // MOV M, A
			case 0x78: reg->a = reg->b; break;											// MOV A, B
			case 0x79: reg->a = reg->c; break;											// MOV A, C
			case 0x7a: reg->a = reg->d; break;											// MOV A, D
			case 0x7b: reg->a = reg->e; break;											// MOV A, E
			case 0x7c: reg->a = reg->h; break;											// MOV A, H
			case 0x7d: reg->a = reg->l; break;											// MOV A, L
			case 0x7e: reg->a = CONCAT(reg->h, reg->l); break;							// MOV A, M
			case 0x7f: reg->a = reg->a; break;											// MOV A, A
		   	//-----------------------------------------------------------------------------------//

			// 8-BIT ARITHMETIC/LOGICAL INSTRUCTIONS
			case 0x3c: reg->a++; break;													// INR A
			case 0x04: reg->b++; break;													// INR B
			case 0x0c: reg->c++; break;													// INR C
			case 0x14: reg->d++; break;													// INR D
			case 0x1c: reg->e++; break;													// INR E
			case 0x24: reg->h++; break;													// INR H
			case 0x2c: reg->l++; break;													// INR L
			case 0x34: set_reg_pair(reg, HL, (CONCAT(reg->h, reg->l) + 1)); break;		// INR M
			case 0x3d: reg->a--; break;													// DCR A
			case 0x05: reg->b--; break;													// DCR B
			case 0x0d: reg->c--; break;													// DCR C
			case 0x15: reg->d--; break;													// DCR D
			case 0x1d: reg->e--; break;													// DCR E
			case 0x25: reg->h--; break;													// DCR H
			case 0x2d: reg->l--; break;													// DCR L
			case 0x35: set_reg_pair(reg, HL, (CONCAT(reg->h, reg->l) - 1)); break;		// DCR M
			case 0x07: rlc(reg); break;													// RLC
			case 0x17: ral(reg); break;													// RAL
			case 0x0f: rrc(reg); break;													// RRC
			case 0x1f: rar(reg); break;													// RAR
			case 0x37: reg->flags.c = 1; break;											// STC
			case 0x2f: reg->a = ~reg->a; break;											// CMA
			case 0x3f: reg->flags.c = ~reg->flags.c; break;								// CMC
			case 0x80: reg->a += reg->b; break;											// ADD B
			case 0x81: reg->a += reg->c; break;											// ADD C
			case 0x82: reg->a += reg->d; break;											// ADD D
			case 0x83: reg->a += reg->e; break;											// ADD E
			case 0x84: reg->a += reg->h; break;											// ADD H
			case 0x85: reg->a += reg->l; break;											// ADD L
			case 0x86: reg->a += read_byte(mem, CONCAT(reg->h, reg->l)); break;			// ADD M
			case 0x87: reg->a += reg->a; break;											// ADD A
            case 0x88: reg->a += reg->b + reg->flags.c; break;                          // ADC B
            case 0x89: reg->a += reg->c + reg->flags.c; break;                          // ADC C
            case 0x8a: reg->a += reg->d + reg->flags.c; break;                          // ADC D
            case 0x8b: reg->a += reg->e + reg->flags.c; break;                          // ADC E
            case 0x8c: reg->a += reg->h + reg->flags.c; break;                          // ADC H
            case 0x8d: reg->a += reg->l + reg->flags.c; break;                          // ADC L
            case 0x8e: reg->a += read_byte(mem, CONCAT(reg->h, reg->l)) + reg->flags.c; break;  // ADC M
            case 0x8f: reg->a += reg->a + reg->flags.c; break;                          // ADC A 
			case 0x90: reg->a -= reg->b; break;											// SUB B
			case 0x91: reg->a -= reg->c; break;											// SUB C
			case 0x92: reg->a -= reg->d; break;											// SUB D
			case 0x93: reg->a -= reg->e; break;											// SUB E
			case 0x94: reg->a -= reg->h; break;											// SUB H
			case 0x95: reg->a -= reg->l; break;											// SUB L
			case 0x96: reg->a -= read_byte(mem, CONCAT(reg->h, reg->l)); break;			// SUB M
			case 0x97: reg->a -= reg->a; break;											// SUB A
			case 0x98: reg->a -= reg->b - reg->flags.c; break;                          // SBB B
            case 0x99: reg->a -= reg->c - reg->flags.c; break;                          // SBB C
            case 0x9a: reg->a -= reg->d - reg->flags.c; break;                          // SBB D
            case 0x9b: reg->a -= reg->e - reg->flags.c; break;                          // SBB E
            case 0x9c: reg->a -= reg->h - reg->flags.c; break;                          // SBB H
            case 0x9d: reg->a -= reg->l - reg->flags.c; break;                          // SBB L
            case 0x9e: reg->a -= read_byte(mem, CONCAT(reg->h, reg->l)) - reg->flags.c; break;  // SBB M
            case 0x9f: reg->a -= reg->a - reg->flags.c; break;                          // SBB A
			case 0xa0: reg->a &= reg->b; break;											// ANA B
			case 0xa1: reg->a &= reg->c; break;                                         // ANA C
			case 0xa2: reg->a &= reg->d; break;                                         // ANA D		   
			case 0xa3: reg->a &= reg->e; break;                                         // ANA E
			case 0xa4: reg->a &= reg->h; break;                                         // ANA H
			case 0xa5: reg->a &= reg->l; break;                                         // ANA L
			case 0xa6: reg->a &= read_byte(mem, CONCAT(reg->h, reg->l)); break;         // ANA M
			case 0xa7: reg->a &= reg->a; break;	// ANA A
			case 0xa8: reg->a ^= reg->b; break;	// XRA B
			case 0xa9: reg->a ^= reg->c; break;	// XRA C
			case 0xaa: reg->a ^= reg->d; break;	// XRA D
			case 0xab: reg->a ^= reg->e; break;	// XRA E
			case 0xac: reg->a ^= reg->h; break;	// XRA H
			case 0xad: reg->a ^= reg->l; break;	// XRA L
			case 0xae: reg->a ^= read_byte(mem, CONCAT(reg->h, reg->l)); break;	// XRA M
			case 0xaf: reg->a ^= reg->a; break;	// XRA A
		}

		printf("mem[0x%08hx] | opcode: %02hhx | cycles: %d\n", reg->pc, reg->ir, OPCODES_CYCLES[reg->ir]);

		if (OPCODES_ARGS[reg->ir] == 1) {
			reg->pc++;
		}
		else if (OPCODES_ARGS[reg->ir] == 2) {
			reg->pc+=2;
		}
		else {
			;
		}

		reg->pc++;

		counter -= OPCODES_CYCLES[reg->ir];

		if (counter <= 0){
			break;
		}
	}

	//test(mem, reg-> fp, offset); // executes registers and memory test
	dump_registers(reg);
	printf("cycles after execution: %d\n", counter);
	dump_memory(mem);

	free(mem);

	return 0;
}
