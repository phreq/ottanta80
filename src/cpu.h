#include <stdint.h>
#include <stdio.h>
#include "memory.h"

// Defines for set_reg_pair() cases
#define BC 0
#define DE 1
#define HL 2
#define SP 3

// Macro for concatenating two registers like H,L as HL
#define CONCAT(HI, LO) ((((HI) << 8) | ((LO) & 0XFF)) & 0XFFFF)

typedef struct status_flags {
    uint8_t s:1;  // sign
    uint8_t z:1;  // zero
    uint8_t ac:1; 
    uint8_t p:1;  // parity
    uint8_t c:1;  // carry
} status_flags;

typedef struct registers {
    uint8_t a;
    uint8_t b;
    uint8_t c;
    uint8_t d;
    uint8_t e;
    uint8_t h;
    uint8_t l;

    uint8_t ir;  // instruction register

    uint16_t sp; // stack pointer
    uint16_t pc; // program counter

    struct status_flags flags;
} registers;

static const uint8_t OPCODES_CYCLES[256] = {
//  0  1   2   3   4   5   6   7   8  9   A   B   C   D   E  F
    4, 10, 7,  5,  5,  5,  7,  4,  4, 10, 7,  5,  5,  5,  7, 4,  // 0
    4, 10, 7,  5,  5,  5,  7,  4,  4, 10, 7,  5,  5,  5,  7, 4,  // 1
    4, 10, 16, 5,  5,  5,  7,  4,  4, 10, 16, 5,  5,  5,  7, 4,  // 2
    4, 10, 13, 5,  10, 10, 10, 4,  4, 10, 13, 5,  5,  5,  7, 4,  // 3
    5, 5,  5,  5,  5,  5,  7,  5,  5, 5,  5,  5,  5,  5,  7, 5,  // 4
    5, 5,  5,  5,  5,  5,  7,  5,  5, 5,  5,  5,  5,  5,  7, 5,  // 5
    5, 5,  5,  5,  5,  5,  7,  5,  5, 5,  5,  5,  5,  5,  7, 5,  // 6
    7, 7,  7,  7,  7,  7,  7,  7,  5, 5,  5,  5,  5,  5,  7, 5,  // 7
    4, 4,  4,  4,  4,  4,  7,  4,  4, 4,  4,  4,  4,  4,  7, 4,  // 8
    4, 4,  4,  4,  4,  4,  7,  4,  4, 4,  4,  4,  4,  4,  7, 4,  // 9
    4, 4,  4,  4,  4,  4,  7,  4,  4, 4,  4,  4,  4,  4,  7, 4,  // A
    4, 4,  4,  4,  4,  4,  7,  4,  4, 4,  4,  4,  4,  4,  7, 4,  // B
    5, 10, 10, 10, 11, 11, 7,  11, 5, 10, 10, 10, 11, 17, 7, 11, // C
    5, 10, 10, 10, 11, 11, 7,  11, 5, 10, 10, 10, 11, 17, 7, 11, // D
    5, 10, 10, 18, 11, 11, 7,  11, 5, 5,  10, 4,  11, 17, 7, 11, // E
    5, 10, 10, 4,  11, 11, 7,  11, 5, 5,  10, 4,  11, 17, 7, 11  // F
};

static const uint8_t OPCODES_ARGS[256] = {
//  0 = no arguments, 1 = single byte argument, 2 = double byte argument

//  0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
	0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 0
	0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, // 1
	0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, // 2
	0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, // 3
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 4
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 5
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 6
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 7
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 8
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 9
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // A
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // B
	0, 0, 2, 2, 2, 0, 1, 0, 0, 0, 2, 2, 2, 2, 1, 0, // C
	0, 0, 2, 1, 2, 0, 1, 0, 0, 0, 2, 1, 2, 2, 1, 0, // D
	0, 0, 2, 0, 2, 0, 1, 0, 0, 0, 2, 0, 2, 2, 1, 0, // E
	0, 0, 2, 0, 2, 0, 1, 0, 0, 0, 2, 0, 2, 2, 1, 0  // F
};

void set_reg_pair(registers *reg, uint8_t reg_pair, uint8_t data);
int load_into_mem(uint8_t *mem, FILE *fp, uint16_t address);
long get_file_size(FILE *fp);
int get_cycles(uint8_t *mem, FILE *fp, uint16_t offset);
void test(uint8_t *mem, registers *reg, FILE *fp, uint16_t offset);
void dump_registers(registers *reg);

// (MORE) COMPLEX INSTRUCTIONS DEFINITIONS

void rlc(registers *reg);
void ral(registers *reg);
void rrc(registers *reg);
void rar(registers *reg);
