#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

#define MEM_SIZE 0x10000 // 65536 in hexadecimal

static uint8_t *mem = NULL;

void write_byte(uint8_t *mem, uint16_t addr, uint8_t data);
uint8_t read_byte(uint8_t *mem, uint16_t addr);
void write_word(uint8_t *mem, uint16_t addr, uint16_t data);
uint16_t read_word(uint8_t *mem, uint16_t addr);
void dump_memory(uint8_t *mem);
