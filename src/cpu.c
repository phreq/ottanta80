#include <stdio.h>
#include <stdint.h>
#include <stdlib.h> // for exit();
#include "cpu.h"

void set_reg_pair(registers *reg, uint8_t reg_pair, uint8_t data) {
	uint8_t hi = (data >> 8) & 0xFF;
	uint8_t lo = data & 0xFF;

	switch (reg_pair) {
		case BC:
			reg->b = hi;
			reg->c = lo;
			break;
		case DE:
			reg->d = hi;
			reg->e = lo;
			break;
		case HL:
			reg->h = hi;
			reg->l = lo;
			break;
		case SP:
			reg->sp = CONCAT(hi, lo);
			break;
		default:
			fprintf(stderr, "Invalid register pair %d\n", reg_pair);
			exit(1);
	}	
}

long get_file_size(FILE *fp) {
	long fsize;	

	fseek(fp, 0, SEEK_END);
	fsize = ftell(fp);
	rewind(fp);

	return fsize;
}

int load_into_mem(uint8_t *mem, FILE *fp, uint16_t addr) {
	uint8_t *addr_ptr;                      // pointer to mem[address]
	size_t result;
	long fsize = get_file_size(fp);

	addr_ptr = &mem[addr];

	result = fread(addr_ptr, 1, fsize, fp);
	if (result != fsize) {
		fputs("Reading error\n", stderr); 
		return 1;
	}	

	return 0;
}

int get_cycles(uint8_t *mem, FILE *fp, uint16_t offset) {
	int cycles = 0;
	uint8_t opcode;

	for (int i = offset; i < (get_file_size(fp) + offset) ; i++) {  // calculate cpu cycles from the cycles table iterating through memory
		opcode = mem[i];
		cycles += OPCODES_CYCLES[opcode];

		if (OPCODES_ARGS[opcode] == 1) {
			i++;
		} 
		else if (OPCODES_ARGS[opcode] == 2) {
			i+=2;
		} 
		else {
			;
		}
	}

	return cycles;
}

void test(uint8_t *mem, registers *reg, FILE *fp, uint16_t offset) {
	reg->a = 1;
	reg->b = 2;
	reg->c = 3;
	reg->d = 4;
	reg->e = 5;
	reg->h = 6;
	reg->l = 7;

	for (reg->pc = offset; reg->pc < (get_file_size(fp) + offset); reg->pc++) {
		reg->ir = mem[reg->pc];
	}

	printf("Program counter and Instruction register [PC, IR] have been iterated!\n");
}

void dump_registers(registers *reg) {
	printf("A: %u\tB: %u\tC: %u\tD: %u\nE: %u\tH: %u\tL: %u\t", reg->a, reg->b, reg->c, reg->d, reg->e, reg->h, reg->l);
	printf("BC: %u\tDE: %u\tHL: %u\n", CONCAT(reg->b, reg->c), CONCAT(reg->d, reg->e), CONCAT(reg->h, reg->l));
	printf("flags: s:%d z:%d ac:%d p:%d c:%d\n", reg->flags.s, reg->flags.z, reg->flags.ac, reg->flags.p, reg->flags.c);
}
