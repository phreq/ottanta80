#include "memory.h"

void write_byte(uint8_t *mem, uint16_t addr, uint8_t data) {
	mem[addr] = data;
}

uint8_t read_byte(uint8_t *mem, uint16_t addr) {
	return mem[addr];
}

void write_word(uint8_t *mem, uint16_t addr, uint16_t data) {
	int hi = (data >> 8) & 0xFF;
	int lo = data & 0xFF;

	mem[addr] = lo;
	mem[addr+1] = hi;
}

uint16_t read_word(uint8_t *mem, uint16_t addr) {
	int hi = mem[addr+1] & 0xFF;
	int lo = mem[addr] & 0xFF;

	return (hi << 8) | lo; 
}

void dump_memory(uint8_t *mem) {
	for (int i = 0; i < MEM_SIZE; i++)
		printf("[%u] %02hhx\n", i, mem[i]);
}
